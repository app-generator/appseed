# React, Vue [App Generator](https://appseed.us/) 
AppSeed - The official repository

## What is AppSeed
AppSeed is a Full-Stack **App Generator** that allows you to choose a visual theme and apply it on a full, but flexible, technology stack in just a few minutes, greatly improving development time from visual design to application deployment.

![AppSeed Intro](https://bitbucket.org/app-generator/appseed/raw/2d11118050c94e479f6ed020fdcbb83946a4b77c/screenshots/appseed-intro.gif)

## Service Resources
Build React, Vue appsin no-time.
- React, Vue [App Generator](https://appseed.us/app-generator) - The app generator ships React, Vue frontends bundled with Express, Flask or Laravel in the backend. 
- Pre-built apps - React, Vue, Flask Apps, GatsbyJS, Apps built with Bulma CSS


## [React Apps](https://appseed.us/apps/react)
React Apps built by AppSeed
- [Material Kit](https://www.creative-tim.com/product/material-kit) - the famous design from **Creative-Tim** combined with Flask, Laravel and Express

## [Vue Apps](https://appseed.us/apps/vuejs)
Vue Apps built by AppSeed
- [Argon Design](https://www.creative-tim.com/product/argon-design-system) - the famous design from **Creative-Tim** combined with Flask, Laravel and Express

## Backend starters
Stable and tested starters, enhanced with database, orm, JWT authenticatication.
- Flask
- Laravel
- Node / Express

## [GatsbyJS Apps](https://appseed.us/apps/gatsbyjs)
GatsbyJS Apps coded by AppSeed
- [HTML5 Up Dimension](https://appseed.us/apps/gatsbyjs/html5up-dimension) - HTML5 Up Design

## [JAMstack Apps](https://appseed.us/apps/jamstack)
JAMstack Apps coded by AppSeed
- [Fractal](https://appseed.us/apps/jamstack/html5up-fractal) - HTML5 Up Design
- [Big Picture](https://appseed.us/apps/jamstack/html5up-big-picture) - HTML5 Up Design
- [Landed](https://appseed.us/apps/jamstack/html5up-landed) - HTML5 Up Design

## [Bulma CSS Apps](https://appseed.us/apps/bulma-css)
Apps made with Bulma CSS
- [BulmaPlay](https://appseed.us/apps/bulma-css/bulmaplay) - AppSeed Design
- [BulmaLanding](https://appseed.us/apps/bulma-css/bulmalanding) - AppSeed Design

## [Flask Apps](https://appseed.us/apps/bulma-css)
Apps coded in Flask
- [BulmaPlay](https://appseed.us/apps/flask-apps/bulmaplay-flask-and-bulma-css) - HTML5 Up Design
- [Solid State](https://appseed.us/apps/flask-apps/flask-solid-state) - HTML5 Up Design
- [PhantomFlask](https://appseed.us/apps/flask-apps/html5up-phantom-coded-in-flask) - HTML5 Up Design


## [Support](https://appseed.us/support)
Via [Discord](https://discord.gg/fZC6hup), [Facebook](https://www.facebook.com/groups/fullstack.apps.generator) and email ** support [ @ ] appseed.us 


---
Provided by AppSeed App Generator




